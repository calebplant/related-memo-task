# Related Memo

## Overview

An org configuration that links a custom memo object that can be linked to Account and Contact. When a memo is created on Account, it is also linked the the Account's contacts via a junction object. When a memo is created on Contact, it adds the Contact's parent Account as the lookup on the memo.

## Demo (~1.5 min)

[Here's a link to a demo video showing the automation in action](https://www.youtube.com/watch?v=pElrl1gRayg)

## Automation Overview

As this seemed like a good opportunity to practice working with flows, I went with a no-code solution. This involved adding a junction object, a record-triggered flow on Memorandum create, and a record-triggered flow on Contact Memo (junction object) create.

### Schema

![Memo Schema](media/schema.png)

### Memorandum Create Flow

Note that the filter condition to trigger this flow is that the memo has a related Account.

![Memorandum Create Flow](media/memorandum-flow.png)

### Contact Memo Create Flow

![Contact Memo Create Flow](media/contact-memo-flow.png)
