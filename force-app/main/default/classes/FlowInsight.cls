public with sharing class FlowInsight {
    @InvocableMethod(label='Show Passed Data')
    public static void showPassedData(List<sObject> objs)
    {
        System.debug('START showPassedData');
        for(sObject eachObj : objs) {
            System.debug(eachObj);
        }
    }
}
