@isTest
public with sharing class MemoFlow_Test {

    private static String ACC_WITH_2_CON_NAME = 'acc1';
    private static String ACC_WITH_NO_CONTACT_NAME = 'acc2';
    private static String CON_WITH_2_CON_ACC_NAME = 'con1';
    private static String CON_WITH_NO_ACC_NAME = 'con3';

    @TestSetup
    static void makeData(){
        List<Account> accs = new List<Account>();
        accs.add(new Account(Name=ACC_WITH_2_CON_NAME)); // 2 contacts
        accs.add(new Account(Name=ACC_WITH_NO_CONTACT_NAME)); // no contacts
        insert accs;
        List<Contact> cons = new List<Contact>();
        cons.add(new Contact(LastName=CON_WITH_2_CON_ACC_NAME, AccountId=accs[0].Id));
        cons.add(new Contact(LastName='con2', AccountId=accs[0].Id));
        cons.add(new Contact(LastName='con3')); // no account
        insert cons;
    }

    /* Account Memo Tests */
    @isTest
    static void doesCreateAccountMemoSetOriginId()
    {
        Account acc = [SELECT Id FROM Account WHERE Name = :ACC_WITH_2_CON_NAME];
        Memorandum__c memo = new Memorandum__c(Name='name1', Title__c='title1', Memo__c='body1');
        insert memo;
        Account_Memo__c accMemo = new Account_Memo__c(Account__c = acc.Id, Memorandum__c = memo.Id);

        Test.startTest();
        insert accMemo;
        Test.stopTest();

        // // Print Data
        // List<Memorandum__c> memos = [SELECT Id, Name, Origin_Id__c FROM Memorandum__c];
        // List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        // List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        // printList(memos);
        // printList(accMemos);
        // printList(conMemos);

        memo = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1' LIMIT 1];
        List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(acc.Id, memo.Origin_Id__c);
        System.assertEquals(1, accMemos.size());
        System.assertEquals(2, conMemos.size());
    }

    @isTest
    static void doesDeleteAccountMemoDeleteAllIfOriginId()
    {
        Account acc = [SELECT Id FROM Account WHERE Name = :ACC_WITH_2_CON_NAME];
        Memorandum__c memo = new Memorandum__c(Name='name1', Title__c='title1', Memo__c='body1');
        insert memo;
        Account_Memo__c accMemo = new Account_Memo__c(Account__c = acc.Id, Memorandum__c = memo.Id);
        insert accMemo;

        List<Memorandum__c> memos = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertNotEquals(0, memos.size());
        System.assertNotEquals(0, accMemos.size());
        System.assertNotEquals(0, conMemos.size());

        Test.startTest();
        delete accMemo;
        Test.stopTest();

        memos = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(0, memos.size());
        System.assertEquals(0, accMemos.size());
        System.assertEquals(0, conMemos.size());
    }

    @isTest
    static void doesDeleteAccountMemoDeleteOnlySelfIfNotOriginId()
    {
        Contact con = [SELECT Id FROM Contact WHERE Name = :CON_WITH_2_CON_ACC_NAME];
        Memorandum__c memo = new Memorandum__c(Name='name1', Title__c='title1', Memo__c='body1');
        insert memo;
        Contact_Memo__c conMemo = new Contact_Memo__c(Contact__c = con.Id, Memorandum__c = memo.Id);
        insert conMemo;

        List<Memorandum__c> memos = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(1, memos.size());
        System.assertEquals(1, accMemos.size());
        System.assertEquals(1, conMemos.size());

        Test.startTest();
        delete accMemos[0];
        Test.stopTest();

        memos = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(1, memos.size());
        System.assertEquals(0, accMemos.size());
        System.assertEquals(1, conMemos.size());
    }

    /* Contact Memo Tests */
    @isTest
    static void doesCreateContactMemoSetOriginId()
    {
        Contact con = [SELECT Id FROM Contact WHERE Name = :CON_WITH_2_CON_ACC_NAME];
        Memorandum__c memo = new Memorandum__c(Name='name1', Title__c='title1', Memo__c='body1');
        insert memo;
        Contact_Memo__c conMemo = new Contact_Memo__c(Contact__c = con.Id, Memorandum__c = memo.Id);

        Test.startTest();
        insert conMemo;
        Test.stopTest();

        // Print Data
        // List<Memorandum__c> memos = [SELECT Id, Name, Origin_Id__c FROM Memorandum__c];
        // List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        // List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        // printList(memos);
        // printList(accMemos);
        // printList(conMemos);

        memo = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(con.Id, memo.Origin_Id__c);
        System.assertEquals(1, accMemos.size());
        System.assertEquals(1, conMemos.size());
    }

    @isTest
    static void doesDeleteContactMemoDeleteAllIfOriginId()
    {
        Account acc = [SELECT Id FROM Account WHERE Name = :ACC_WITH_2_CON_NAME];
        Memorandum__c memo = new Memorandum__c(Name='name1', Title__c='title1', Memo__c='body1');
        insert memo;
        Account_Memo__c accMemo = new Account_Memo__c(Account__c = acc.Id, Memorandum__c = memo.Id);
        insert accMemo;

        List<Memorandum__c> memos = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(1, memos.size());
        System.assertEquals(1, accMemos.size());
        System.assertEquals(2, conMemos.size());

        Test.startTest();
        delete conMemos[0];
        Test.stopTest();

        memos = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(1, memos.size());
        System.assertEquals(1, accMemos.size());
        System.assertEquals(1, conMemos.size());
    }

    @isTest
    static void doesDeleteContacttMemoDeleteOnlySelfIfNotOriginId()
    {
        Contact con = [SELECT Id FROM Contact WHERE Name = :CON_WITH_2_CON_ACC_NAME];
        Memorandum__c memo = new Memorandum__c(Name='name1', Title__c='title1', Memo__c='body1');
        insert memo;
        Contact_Memo__c conMemo = new Contact_Memo__c(Contact__c = con.Id, Memorandum__c = memo.Id);
        insert conMemo;

        List<Memorandum__c> memos = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(1, memos.size());
        System.assertEquals(1, accMemos.size());
        System.assertEquals(1, conMemos.size());

        Test.startTest();
        delete accMemos[0];
        Test.stopTest();

        memos = [SELECT Id, Origin_Id__c FROM Memorandum__c WHERE Name = 'name1'];
        accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        System.assertEquals(1, memos.size());
        System.assertEquals(0, accMemos.size());
        System.assertEquals(1, conMemos.size());
    }

    private static void printData()
    {
        List<Memorandum__c> memos = [SELECT Id, Name, Origin_Id__c FROM Memorandum__c];
        List<Account_Memo__c> accMemos = [SELECT Id, Account__c, Memorandum__c FROM Account_Memo__c];
        List<Contact_Memo__c> conMemos = [SELECT Id, Contact__c, Memorandum__c FROM Contact_Memo__c];
        printList(memos);
        printList(accMemos);
        printList(conMemos);
    }

    private static void printList(List<Sobject> objs)
    {
        if(objs.size() > 0) {
            System.debug('Print ' + objs[0].getSobjectType());
            for(sObject eachObj : objs) {
                System.debug(eachObj);
            }
        }
    }
}
