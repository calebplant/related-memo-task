public with sharing class RelatedMemoQueueable implements Queueable {

    private Id memoIdToDelete;

    public RelatedMemoQueueable(Id memoId)
    {
        memoIdToDelete = memoId;
    }

    public void execute(QueueableContext context)
    {
        System.debug('EXECUTE RelatedMemoQueueable');
        Database.delete(memoIdToDelete);
    }

    @InvocableMethod(label='Enqueue Memo Deletion')
    public static void enqueMemoDelete(List<MemoQueueableInputs> memosToDelete)
    {
        System.debug('START enqueMemoDelete');
        System.debug(memosToDelete);
        for(MemoQueueableInputs eachMemo : memosToDelete) {
            RelatedMemoQueueable memoQueueable = new RelatedMemoQueueable(eachMemo.passedMemoId);
            System.enqueueJob(memoQueueable);
        }
    }

    public with sharing class MemoQueueableInputs {
        @InvocableVariable(label='Memorandum Id' required=true)
        public Id passedMemoId;
    }
}
