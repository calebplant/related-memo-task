public with sharing class RelatedMemoUpsert {
    @InvocableMethod(label='Upsert Memo Links')
    public static void upsertMemoLinks(List<UpsertMemosInputs> inputs)
    {
        System.debug('START upsertMemoLinks');
        System.debug(inputs);

        if(inputs[0].passedLinks.size() > 0) {
            Schema.sObjectField keyField = inputs[0].passedLinks[0].getSobjectType().getDescribe().fields.getMap().get('Compound_Key__c');

            List<sObject> records = new List<sObject>();
            for(UpsertMemosInputs eachInput : inputs) {
                records.addAll(eachInput.passedLinks);
            }
            Database.upsert(records, keyField);
        }
    }

    public with sharing class UpsertMemosInputs {
        @InvocableVariable(label='Memorandum Links' required=true)
        public List<sObject> passedLinks;
    }
}
